from telegrambot.src.models import db
from datetime import datetime
from telegrambot.src.crypto import aescrypt
from telegrambot.lms.login import get_token
from dateutil.relativedelta import relativedelta
import time

def checker():
    det = datetime.now()
    x = db.session.find({'expire_at':{'$lt':det}})
    for session in x:
        username = session['student_number']
        hashed_password = session['password']
        password = aescrypt.decrypt(hashed_password)
        chatid = session['chatid']
        login = get_token(username, password)
        if login != False:
            token = login['.ASPXAUTH']
            exptime = datetime.now() + relativedelta(days=10)
            db.session.update_one({'chatid':chatid}, {'$set':{'session':token,'expire_at':exptime}})
        else:
            db.session.delete_one({'chatid':chatid})
            print('password changed user removed from session!')


def pool():
    while True:
        checker()
        hour = 3600 * 2
        time.sleep(hour)
