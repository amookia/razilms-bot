from pymongo import MongoClient
import requests
import config
from jdatetime import datetime
import time
from threading import Thread
from telegrambot.lms.crawler import ( details , date_beautify , time_beutify )
from bs4 import BeautifulSoup
from jobs.refresher import pool


#INIT
#database configuration
Thread(target=pool).start()
client = MongoClient('mongodb')
db = client['lmsbot']

#Send init message to admin
rem = f'https://api.telegram.org/bot{config.token}/sendMessage?chat_id=admin_chat_Id&text=' + 'started at ' +  str(datetime.now().strftime('%H:%M'))
requests.get(rem)



def all_lessons(token,chatid):
    get_co = details({'.ASPXAUTH':token})['lessons']
    text = str()
    if get_co is not None:
        all_list = list()
        for co in get_co:
            link = config.lms_url + 'VirtualAdmin' + co['link']
            link += 'iisijisjdas'
            removestr = '/' + link.split('/')[-1]
            link = link.replace(removestr,'')
            get_html = requests.get(link,cookies={'.ASPXAUTH':token})
            bs = BeautifulSoup(get_html.text,'lxml')
            table = bs.find('div',{'class':'grid-parent'}).find('table').find_all('tr',{'class':'examrow'})
            course_name = bs.find('a',{'class':'NodeCssClass'})
            course_name = course_name.text.replace('- گلستان','')
            for i in table:
                try:
                    date = i.find_all('span')[1].text
                    time = i.find('td',{'width':'65'}).text.replace('  ','').replace('\n','')
                    compdate = date_beautify(date)
                    comptime = time_beutify(time)
                    persian_time = compdate + ' - ' +  comptime
                    pdtime = datetime.strptime(persian_time,'%Y/%m/%d - %H:%M')

                    #get 23:59 timestamp
                    datetimenow = str(datetime.now().strftime('%Y/%m/%d')) + ' - 23:59'
                    maxtime = int(datetime.strptime(datetimenow,'%Y/%m/%d - %H:%M').timestamp())
                    pdt = int(pdtime.timestamp())
                    now = int(datetime.now().timestamp())
                    if now < pdt < maxtime:
                        all_list.append({'course_name':course_name,'comptime':comptime,
                        'realtime':date,'timestamp':pdt})
                except Exception as e:
                    print(e)


        all_list = sorted(all_list, key=lambda x : x['timestamp'])
        if len(all_list) != 0 : text += '❗️یادآوری کلاس‌های امروز❗️' + '\n\n'
        for obj in all_list:
            text +=  '📍 ' + obj['course_name'] + '\n' + '🗓 ' + obj['realtime'] + '\n' + \
            '⏰ زمان : ' + obj['comptime'] + '\n\n'

        text += '@RaziLMSbot\n.'
        #send text to id
        url = f'https://api.telegram.org/bot{config.token}/sendMessage?chat_id={str(chatid)}&text=' + text
        requests.get(url)




while True:
    nowtime = str(datetime.now().strftime('%H:%M'))
    if nowtime == '06:00':
        active_users = db.reminder_status.find({'active':True})
        for user in active_users:
            token = db.session.find_one({'chatid':user['chatid']})['session']
            Thread(target=all_lessons,args=[token,user['chatid']]).start()

    time.sleep(60)
