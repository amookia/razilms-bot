from flask import (Blueprint , render_template , request , session , url_for , redirect , flash)
import admin.forms as forms
import config
from functools import wraps
from telegrambot.src.models import db
from admin.src import (get_user , Send_Message)
from threading import Thread

admin_app = Blueprint('admin',__name__,url_prefix='/amookia/admin',
template_folder='./views/templates',static_folder='./views/static',static_url_path='/static')


def is_admin(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'username' in session:
            if session['username'] != config.admin_username:
                return redirect(url_for('admin.login'))
        else:
            return redirect(url_for('admin.login'))
        return f(*args, **kwargs)
    return wrap




@admin_app.route('/login',methods=['POST','GET'])
def login():
    if 'username' in session:
        if session['username'] == config.admin_username:
            return redirect(url_for('admin.panel'))
    form = forms.LoginForm()
    postform = request.form
    if form.validate_on_submit():
        username = postform['username']
        password = postform['password']
        isuserok = username == config.admin_username
        ispwok = password == config.admin_password
        if isuserok and ispwok:
            session['username'] = request.form['username']
            return redirect(url_for('admin.panel'))
        else:
            flash('Username or Password is wrong')
    if 'recaptcha' in form.errors:
        flash('Recaptcha missed!')
    return render_template('login/login.html',form=form)

@admin_app.route('/logout')
@is_admin
def logout():
    session.pop('username')
    return redirect(url_for('admin.login'))



@admin_app.route('/panel')
@is_admin
def panel():
    findall_users = db.members.count()
    findall_session = db.session.count()
    findall_reminder = db.reminder_status.count({'active':True})
    all_sessions = list(db.session.find())[::-1]
    ratio = str(round(findall_session/findall_users * 100,1))

    return render_template('panel/panel.html',totalusers=findall_users,
    totalsessions=findall_session,ratio=ratio,
    details=all_sessions[:5])



@admin_app.route('/sessions')
@is_admin
def sessions():
    all_sessions = db.session.find()
    return render_template('panel/sessions.html',details=all_sessions)



@admin_app.route('/find')
@is_admin
def find():
    getargs = request.args
    if getargs is not None and 'chatid' in getargs:
        user = get_user(getargs['chatid'])
        if user['ok']:
            text = str()
            result = user['result']
            if 'username' in result: text +=  '@' + result['username'] + ' + '
            text += result['first_name']
            flash(text,'alert-success')
        else:
            flash('Not found','alert-danger')
    return render_template('panel/find.html',text='HELLO')


@admin_app.route('/removesession/<int:chatid>')
@is_admin
def removesession(chatid):
    find = db.session.find_one({'chatid':chatid})
    if find is not None:
        db.session.delete_one({'chatid':chatid})
        db.reminder_status.delete_one({'chatid':chatid})
        flash(f'{str(chatid)} removed from db!','alert-danger')
    else:
        flash(f'{str(chatid)} Not found!','alert-warning')
    return render_template('panel/sessions.html')


@admin_app.route('/sendmessage',methods=['POST','GET'])
@is_admin
def SendMessage():
    values = request.form
    method = request.method
    if 'option' in values and 'text' in values and method == 'POST':
        optionList = ['allmembers','verified','nonverified']
        option = values['option']
        text = values['text']
        if option in optionList and len(text) != 0:
            Thread(target=Send_Message,args=[option,text]).start()
            return render_template('panel/sendmessage.html')
    return redirect(url_for('admin.panel'))

@admin_app.route('/members')
@is_admin
def members():
    members = db.members.find()
    return render_template('panel/members.html',datas=members)
