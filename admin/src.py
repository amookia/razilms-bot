import requests
import config
from pythonping import ping
from threading import Thread
from telegrambot.src.models import db


def get_user(chatid):
    url = f'https://api.telegram.org/bot{config.token}/getChat?chat_id={str(chatid)}'
    return requests.get(url).json()

def Send_Message(option,textmsg):
    if option == 'allmembers':
        members = db.members.find()
        for member in members:
            chatid = member['chatid']
            requests.get(f'https://api.telegram.org/bot{config.token}/sendMessage?chat_id={str(chatid)}&text={str(textmsg)}')

    if option == 'verified':
        print('verified')
        vermembers = db.session.find()
        for vermember in vermembers:
            chatid = vermember['chatid']
            requests.get(f'https://api.telegram.org/bot{config.token}/sendMessage?chat_id={str(chatid)}&text={str(textmsg)}')

    if option == 'nonverified':
        simple = str()
        #vermembers = db.session.find()
        #members = db.members.find()
        #for members
