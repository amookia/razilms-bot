from pymongo import MongoClient
import config
import time
import datetime

class mongo:
    def __init__(self):
        client = MongoClient(config.mongouri)
        db = client['lmsbot']
        self.members = db['members']
        self.session = db['session']
        self.reminder = db['reminder']
        self.reminder_status = db['reminder_status']
        self.userlimit = db['user_limit']
        self.banlist = db['banlist']
        self.statics = db['statics']
        self.userlimit.ensure_index('expire', expireAfterSeconds=10*60)
        self.banlist.ensure_index('expire', expireAfterSeconds=3600)
        self.statics.ensure_index('expire', expireAfterSeconds=10*60)
