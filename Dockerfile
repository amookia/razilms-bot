FROM python:buster
copy . /app
WORKDIR /app
ENV TZ=Asia/Tehran
RUN pip3 install requests -r requirements.txt
CMD gunicorn -b 0.0.0.0:80 -w 2 app:app & python reminder.py
