from flask import Flask
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
import config
from datetime import timedelta


sentry_sdk.init(
    dsn="https://79ea4a17f2ce4504a10995ac9c6b4a72@o437963.ingest.sentry.io/5401103",
    integrations=[FlaskIntegration()]
)

app = Flask(__name__)
app.config['SECRET_KEY'] = config.secret
app.config['RECAPTCHA_PRIVATE_KEY'] = '6LfP9sMZAAAAADtWwzO7gzK4l60k1-LG1e13DgNH'
app.config['PERMANENT_SESSION_LIFETIME'] =  timedelta(minutes=15)

from telegrambot.main import botapi
app.register_blueprint(botapi)
from admin.admin import admin_app
app.register_blueprint(admin_app)


if __name__ == '__main__':
    app.run(port=5000,debug=True)
