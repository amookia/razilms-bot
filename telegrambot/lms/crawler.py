import config
from bs4 import BeautifulSoup
import datetime
import requests
import re
import jdatetime
import pytz
from telegrambot.src.models import db
import urllib.parse
from telegrambot.lms.links import LinkGenerator

def date_beautify(text):
    if text is not None:
        months = [{'num':'01','month':'فروردين'},{'num':'02','month':'ارديبهشت'},{'num':'03','month':'خرداد'},
        {'num':'04','month':'تير'},{'num':'05','month':'مرداد'},{'num':'06','month':'شهريور'},{'num':'07','month':'مهر'},
        {'num':'08','month':'آبان'},{'num':'09','month':'آذر'},{'num':'10','month':'دی'},{'num':'11','month':'بهمن'},{'num':'12','month':'اسفند'}]
        days = ['شنبه','يكشنبه','دوشنبه','سه شنبه','چهارشنبه','پنج شنبه','جمعه']
        for mon in months:
            if mon['month'] in text:
                month_text = text.replace(mon['month'],mon['num'])
        date = re.findall("\d+",month_text)
        if int(date[0]) < 10 :
            date_month =  '0' + date[0]
        else:
            date_month = date[0]
        date_time = date[2] + '/' + date[1] + '/' + date_month
        return date_time
    else:
        return None

def time_beutify(text):
    AM = 'ق.ظ'
    PM = 'ب.ظ'
    if AM in text:
        am_time = text.replace(AM,'').replace(' ','').replace('\r','')
        time = datetime.datetime.strptime(am_time,'%H:%M')
        return time.strftime('%H:%M')
    if PM in text:
        pm_time = text.replace(PM,'').replace(' ','').replace('\r','')
        if int(pm_time.split(':')[0]) == 12:
            time = datetime.datetime.strptime(pm_time,'%H:%M')
            return time.strftime('%H:%M')
        else:
            time = datetime.datetime.strptime(pm_time,'%H:%M') + datetime.timedelta(hours=+12)
            return time.strftime('%H:%M')

#Crawl all lessons from webpage
def details(token):

    get_html = requests.get(config.lms_url +'ViewProfile.aspx',cookies=token)
    bs = BeautifulSoup(get_html.text,'lxml')
    base_userinfo = bs.find('fieldset').find('table')
    lastname = base_userinfo.find('span',{'id':'ctl00_mainContent_lblLastName'}).text
    name = base_userinfo.find('span',{'id':'ctl00_mainContent_lblFirstName'}).text
    userinfo = name + ' ' + lastname
    les = bs.find('fieldset',id='ctl00_mainContent_FieldsetEducationalInfo').find('div',id='ctl00_mainContent_divLessons')
    lessons = []
    if les is not None:
        for i in les.find_all('a'):
            if 'گروه' in i.text:
                course = i.text.replace('- گلستان','').replace('  ',' ')
                link = i.attrs['href'].replace('/Lesson','')
                lessons.append({'name':course,'link':link})
    if les is None : lessons = None
    return [{'name':userinfo,'lessons':lessons}][0]


def all_lessons(token,chatid):
    get_co = details({'.ASPXAUTH':token})['lessons']
    text = str()
    if get_co is not None:
        all_list = list()
        for co in get_co:
            link = config.lms_url + 'VirtualAdmin' + co['link']
            link += 'iisijisjdas'
            removestr = '/' + link.split('/')[-1]
            link = link.replace(removestr,'')
            get_html = requests.get(link,cookies={'.ASPXAUTH':token})
            bs = BeautifulSoup(get_html.text,'lxml')
            table = bs.find('div',{'class':'grid-parent'}).find('table').find_all('tr',{'class':'examrow'})
            course_name = bs.find('a',{'class':'NodeCssClass'})
            course_name = course_name.text.replace('- گلستان','')
            for i in table:
                try:
                    date = i.find_all('span')[1].text
                    time = i.find('td',{'width':'65'}).text.replace('  ','').replace('\n','')
                    compdate = date_beautify(date)
                    comptime = time_beutify(time)
                    persian_time = compdate + ' - ' +  comptime
                    pdt = int(jdatetime.datetime.strptime(persian_time,'%Y/%m/%d - %H:%M').timestamp())
                    now = int(jdatetime.datetime.now().timestamp())
                    if pdt > now:
                        all_list.append({'course_name':course_name,'comptime':comptime,
                        'realtime':date,'timestamp':pdt})
                except:
                    pass



        all_list = sorted(all_list, key=lambda x : x['timestamp'])
        for obj in all_list:
            text += '📍 ' + obj['course_name'] + '\n' + '🗓 ' + obj['realtime'] + '\n' + \
            '⏰ زمان : ' + obj['comptime'] + '\n\n'

    if len(text) == 0:
        text += '❗️نتیجه ای یافت نشد.' + '\n\n'

    text += '@RaziLMSbot\n.'
    return text




def my_exams(token,link):
    link += 'iisijisjdas'
    removestr = '/' + link.split('/')[-1]
    link = link.replace(removestr,'')
    exam = requests.get(link,cookies=token)
    bs = BeautifulSoup(exam.text,'lxml')
    founded_exams = bs.find_all('tr',{'class':'gridrow'})
    if len(founded_exams) != 0 :
        course_name = bs.find('a',{'class':'NodeCssClass'})
        course_name = course_name.text.replace('- گلستان','')
        text = '📚 نام درس : ' + course_name + '\n\n'
        for i in founded_exams:
            status = i.find('td',id='ctl00_mainContent_dlstExams_ctl01_tdYourStatus').text
            total_question = i.find('span',id='ctl00_mainContent_dlstExams_ctl01_lblQCount').text
            dates = i.find_all('td', {'class': 'cell cellb'})
            from_time = dates[3].text.replace('  ','').replace('\n','')
            to_time = dates[4].text.replace('  ','').replace('\n','')
            text += '🗓 تاریخ شروع : ' + from_time + '\n' + '🗓 تاریخ پایان : ' + to_time + '\n\n' + f'📍 وضعیت {status}' + '\n\n'

    else:
        text = '❗️نتیجه ای یافت نشد.' + '\n\n'
    return text + '@RaziLMSbot\n.'



def course_detail(token,link):
    link += 'iisijisjdas'
    removestr = '/' + link.split('/')[-1]
    link = link.replace(removestr,'')
    get_html = requests.get(link,cookies=token)
    bs = BeautifulSoup(get_html.text,'lxml')
    try:
        table = bs.find('div',{'class':'grid-parent'}).find('table').find_all('tr',{'class':'examrow'})
        course_name = bs.find('a',{'class':'NodeCssClass'})
        course_name = course_name.text.replace('- گلستان','')
        if table is not None:
            has_confirmed = str()
            text = course_name + '\n\n'
            tz = pytz.timezone('Asia/Tehran')
            for i in table:
                time = i.find('td',{'width':'65'}).text.replace('  ','').replace('\n','')
                date = i.find_all('span')[1].text
                cosession = i.find('span').text.replace(':','')
                persian_time = date_beautify(date) + ' - ' +  time_beutify(time)
                pdt = int(jdatetime.datetime.strptime(persian_time,'%Y/%m/%d - %H:%M').timestamp())
                now = int(jdatetime.datetime.now(tz).timestamp())
                if now > pdt :
                    has_confirmed = '\n' + '✅ برگزار شده'
                    text += '📍 ' + cosession + '\n' + '🗓 ' + date + '\n' + '⏰ ' + time + has_confirmed + '\n\n'
                else:
                    text += '📍 ' + cosession + '\n' + '🗓 ' + date + '\n' + '⏰ ' + time + '\n\n'
            if text == course_name + '\n\n' :
                text = '❗️نتیجه ای یافت نشد.' +'\n\n'
            return text + '@RaziLMSbot\n.'

        else:
            return None

    except:
        table = bs.find('a',{'class':'classLink'})['href']
        regex = '''(?<=\(')(.*?)(?=\')'''
        findp = re.findall(regex, table)
        if findp is not None:
            postback = findp[0]
            token = token['.ASPXAUTH']
            LG = LinkGenerator(token,link)
            links = LG.GetLiveLink(postback)
            if links is not None:
                text = '❗️کلاس در حال برگزاری میباشد.' + '\n\n' + '👇 لینک ورود' + '\n\n' +\
                '@RaziLMSbot\n.' + \
                '''&reply_markup={"inline_keyboard": [[{"text": "↩️ ورود به کلاس","url":"''' + links[0]['link'] + '''","selective":true}]]}'''

            return text


def course_basic(token,link):
    cookie = {'.ASPXAUTH':token}
    resp = requests.get(link,cookies=cookie)
    bs = BeautifulSoup(resp.text,'lxml')
    try:
        teacher_name = bs.find('a',id='ctl00_mainContent_TeachersList1_rptTeachersList_ctl00_TeacherName').text
        course_name = bs.find('span',id='ctl00_mainContent_lblLessonTitle').text
        text = '🔸نام درس : ' + course_name + '\n' + '🔹نام استاد : ' + teacher_name +\
         '\n' + '📍یکی از دکمه های زیر را انتخاب کنید.' + '\n\n@RaziLMSbot'
    except Exception as e:

        text = None

    return text
