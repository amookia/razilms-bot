from bs4 import BeautifulSoup
import requests
import pytz
import urllib.parse
import re

class LinkGenerator:
    def __init__(self,token,link):
        self.token = token
        self.link = link
        self.cookies = {'.ASPXAUTH':self.token}
        self.links = []



    def GetRecorded(self,target):
        data = {'__EVENTTARGET':target,'__VIEWSTATE':self.viewstate,
        'ctl00$mainContent$pcCreateVC$C$txtVCTitle':self.title}
        send = requests.post(self.link,cookies=self.cookies,data=data)
        try:
            javascript = "javascript:__doPostBack('" + target +"','')"
            regex = '''(?<='http:)(.*)(?=')'''
            link = re.findall(regex,send.text)[0].replace('//','')
            bs = BeautifulSoup(send.text,'html.parser')
            names = bs.find_all('a',{'href':javascript})
            if len(names) != 0:
                name = names[0].text
            else:
                name = 'مشاهده ویدیو'
            course_name = bs.find('span',id='ctl00_lblBreadCrump').find('a',{'class':'NodeCssClass'}).text
            self.links.append({'text':course_name,'link':link,'name':name})

        except:
            pass


    def SendPost(self,target,viewstate):
        data = {'__EVENTTARGET':target,'__VIEWSTATE':viewstate}
        send = requests.post(self.link,cookies=self.cookies,data=data)

        bs = BeautifulSoup(send.text,'html.parser')
        videos = bs.find_all('tr',{'class':'gridrow'})
        for video in videos:
            atag = video.find('a')
            href = atag['href']
            regex = '''(?<=\(')(.*?)(?=\')'''
            href = re.findall(regex, href)[0]
            text = atag.text
            self.viewstate = bs.find("input", {"id": "__VIEWSTATE"}).attrs['value']
            self.GetRecorded(href)




    def GetPageVideo(self,target):
        send_get = requests.get(self.link,cookies=self.cookies)
        cookies = {'.ASPXAUTH':self.token,'ASP.NET_SessionId':send_get.cookies['ASP.NET_SessionId'],
        '__AntiXsrfToken':send_get.cookies['__AntiXsrfToken']}

        self.cookies = cookies

        bs = BeautifulSoup(send_get.text,'html.parser')
        self.viewstate = bs.find("input", {"id": "__VIEWSTATE"}).attrs['value']
        self.title = bs.find("input",{"name":"ctl00$mainContent$pcCreateVC$C$txtVCTitle"})['value']
        allvideos = bs.find_all("a",{'title':'مشاهده فایل های ضبط شده'},href=True)
        for x in allvideos:
            regex = '''(?<=\(')(.*?)(?=\')'''
            postback = re.findall(regex, x['href'])[0]
            if postback == target:
                self.SendPost(target,self.viewstate)

        return self.links


    def GetAllNames(self):
        names = list()
        try:
            send_get = requests.get(self.link,cookies=self.cookies)
            bs = BeautifulSoup(send_get.text,'lxml')
            bodies = bs.find_all('tbody')
            for detail in bodies:
                date = detail.find('td',{'width':'200'}).find_all('span')[1].text
                video = detail.find("a",{'title':'مشاهده فایل های ضبط شده'},href=True)
                regex = '''(?<=\(')(.*?)(?=\')'''
                postback = re.findall(regex, video['href'])[0]
                names.append({'date':date,'postback':postback})
        except:
            pass

        return names

    def GetLiveLink(self,target):
        send_get = requests.get(self.link,cookies=self.cookies)
        cookies = {'.ASPXAUTH':self.token,'ASP.NET_SessionId':send_get.cookies['ASP.NET_SessionId'],
        '__AntiXsrfToken':send_get.cookies['__AntiXsrfToken']}
        self.cookies = cookies
        bs = BeautifulSoup(send_get.text,'lxml')
        self.viewstate = bs.find("input", {"id": "__VIEWSTATE"}).attrs['value']
        self.title = bs.find("input",{"name":"ctl00$mainContent$pcCreateVC$C$txtVCTitle"})['value']
        self.GetRecorded(target)
        return self.links
