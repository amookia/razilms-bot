import requests
from bs4 import BeautifulSoup
import config

def get_token(username,password):
    s = requests.Session()
    g = s.get(config.lms_url + 'LoginPage.aspx?ReturnUrl=%2f')

    bs = BeautifulSoup(g.text,'html.parser')
    viewstate = bs.find("input", {"id": "__VIEWSTATE"}).attrs['value']


    post_data = {'__VIEWSTATE':viewstate,'ctl00$mainContent$hdfPass':password,
    'ctl00$mainContent$UserName':username,'ctl00$mainContent$Password':password,'ctl00$mainContent$LoginButton':'ورود'}

    login_lms = s.post(config.lms_url + 'LoginPage.aspx?ReturnUrl=%2f',data=post_data,allow_redirects=False)
    if login_lms.status_code == 302:
        cookies = login_lms.cookies
        retrun_cookies = list()
        retrun_cookies.append({'.ASPXAUTH':cookies['.ASPXAUTH']})
        return cookies
    else:
        return False
