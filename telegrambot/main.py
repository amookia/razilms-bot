from flask import Blueprint,request
from telegrambot.src.tel import bot,channel
from threading import Thread
from telegrambot.src.models import db
import config
import sentry_sdk

botapi = Blueprint('botapi',__name__)


@botapi.route(f'/lmsbotkia/{config.token}',methods=['POST'])
def telegram():
    jsonget = request.json
    try:
        if 'message' in jsonget:
            msgid = jsonget['message']['message_id']
            chatid = jsonget['message']['from']['id']
            textmsg = jsonget['message']['text']

            if textmsg == '/start':
                bot.start_msg(chatid)

            if ':' in textmsg :
                Thread(target=bot.login_user,args=[chatid,textmsg]).start()

            if '🗂 اطلاعات من' == textmsg:
                token = db.session.find_one({'chatid':chatid})
                if token is not None:
                    Thread(target=bot.my_info,args=[chatid]).start()
                else:
                    bot.start_msg(chatid)

            if '📚 درس ها' == textmsg:
                token = db.session.find_one({'chatid':chatid})
                if token is not None:
                    Thread(target=bot.my_lessons,args=[chatid]).start()
                else:
                    bot.start_msg(chatid)

            if '🔔 یادآوری' == textmsg:
                token = db.session.find_one({'chatid':chatid})
                if token is not None:
                    Thread(target=bot.reminder,args=[chatid]).start()
                else:
                    bot.start_msg(chatid)

            if '📙 راهنما' == textmsg:
                bot.help_msg(chatid)

            if '🌎 وضعیت سامانه' == textmsg:
                bot.lms_statics(chatid)

        if 'callback_query' in request.json:
                callback_data = request.json['callback_query']['data']
                callback_chatid = request.json['callback_query']['from']['id']
                callback_messageid  = request.json['callback_query']['message']['message_id']

                if 'ls-' == callback_data[:3]:
                    text = callback_data[3:]
                    Thread(target=bot.list_show,args=[callback_chatid, text]).start()
                    bot.delete_msg(callback_chatid, callback_messageid)

                if 'sl-' == callback_data[:3]:
                    link = callback_data[3:]
                    Thread(target=bot.course_info,args=(callback_chatid,link,'coursedetail')).start()
                    bot.delete_msg(callback_chatid, callback_messageid)

                if 'se-' == callback_data[:3]:
                    link = callback_data[3:]
                    Thread(target=bot.course_info,args=(callback_chatid,link,'examdetail')).start()
                    bot.delete_msg(callback_chatid, callback_messageid)

                if 're-' == callback_data[:3]:
                    link = callback_data[3:]
                    Thread(target=bot.recorded_list,args=(callback_chatid,link)).start()
                    bot.delete_msg(callback_chatid, callback_messageid)

                if '$dl' == callback_data[:3]:
                    text = 'ctl00$mainContent' + callback_data
                    Thread(target=bot.get_recorded,args=(callback_chatid,text)).start()

                if callback_data == 'deletesession':
                    bot.delete_session(callback_chatid,callback_messageid)

                if callback_data == 'activereminder':
                    bot.set_reminder(callback_chatid,callback_messageid,True)
                if callback_data == 'deactivereminder':
                    bot.set_reminder(callback_chatid,callback_messageid,False)

                if callback_data == 'myallclass':
                    Thread(target=bot.myall_lessons,args=[callback_chatid]).start()

        if 'channel_post' in request.json:
            channel_msgid = jsonget['channel_post']['message_id']
            channel_chatid = jsonget['channel_post']['chat']['id']
            Thread(target = channel.forward,args = [channel_chatid,channel_msgid]).start()
    except:
        pass


    return 'OK'
