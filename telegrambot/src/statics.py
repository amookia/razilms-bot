from pythonping import ping        #GetPing
import requests                    #Requests
from telegrambot.src.models import db


class RaziStatics:
    def __init__(self):
        self.sites = [
            'https://lms1.razi.ac.ir/LoginPage.aspx?ReturnUrl=%2f',
            'https://lms2.razi.ac.ir/LoginPage.aspx?ReturnUrl=%2f',
            'https://lms3.razi.ac.ir/LoginPage.aspx?ReturnUrl=%2f',
            'https://lms4.razi.ac.ir/LoginPage.aspx?ReturnUrl=%2f',
            'https://lms5.razi.ac.ir/LoginPage.aspx?ReturnUrl=%2f',
            'https://lms6.razi.ac.ir/LoginPage.aspx?ReturnUrl=%2f',
            'https://lms7.razi.ac.ir/LoginPage.aspx?ReturnUrl=%2f',
            'https://lms8.razi.ac.ir/LoginPage.aspx?ReturnUrl=%2f',
            'https://lms9.razi.ac.ir/LoginPage.aspx?ReturnUrl=%2f',
        ]


    def get_status_code(self):
        lists = []
        count = db.statics.count()
        if count == 0:
            for link in self.sites:
                try:
                    req = requests.get(link,timeout=5)
                    link = link.replace('https://', '').replace('/LoginPage.aspx?ReturnUrl=%2f','')
                    if req.status_code == 200 and len(req.text) != 0:
                        lists.append({'link':link,'text':'✅'})
                    else:
                        lists.append({'link':link,'text':'❌'})
                except:
                    link = link.replace('https://', '').replace('/LoginPage.aspx?ReturnUrl=%2f','')
                    lists.append({'link':link,'text':'❌'})
                count += 1
            db.static_lists(lists)
            return db.statics.find_one()
        else:
            return db.statics.find_one()

RaziStatics = RaziStatics()
