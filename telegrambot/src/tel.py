import requests
import config
from telegrambot.src.models import db
from telegrambot.lms.login import get_token
from telegrambot.lms.crawler import (details,course_detail,my_exams,all_lessons,course_basic)
import json
import urllib.parse
from threading import Thread
from functools import wraps
import datetime
from telegrambot.src.statics import RaziStatics
from telegrambot.lms.links import LinkGenerator




class bot:
    def __init__(self):
        self.api_url = f'https://api.telegram.org/bot{config.token}/'



    def delete_msg(self,chatid,msgid):
        delete_url = self.api_url + f'deleteMessage?chat_id={str(chatid)}&message_id={str(msgid)}'
        requests.get(delete_url)

    def start_msg(self,chatid):
        db.insert_member(chatid)
        find_session = db.session.find_one({'chatid':chatid})
        if find_session is None :
            start_text = '❗️ سشن شما یافت نشد.' + '\n' + 'برای دریافت اطلاعات و ورود به سامانه' + '\n' + 'شماره دانشجویی و رمز عبور خود را با فرمت زیر ارسال کنید.' + '\n\n' + \
            'username:password' + '\n' + 'برای مثال :' + '\n' + '98510000:Abc123' + '\n\n' + 'شماره دانشجویی : 98510000' +\
            '\n' + 'رمز عبور : Abc123' + '\n\n' + 'رمز عبور شما به هیچ وجه در سرور ذخیره نمیشود.' +'\n.'
            url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text={start_text}' + '&reply_markup=' + '''{"resize_keyboard":true,"keyboard":[[{"text":"📙 راهنما"}]]}'''
            requests.get(url)
        else:
            keyboard = '''{"resize_keyboard":%20true,"keyboard":%20[[{"text":%20"🗂%20اطلاعات%20من"},{"text":%20"📚%20درس%20ها"}],[{"text":"📙 راهنما"},{"text":"🔔 یادآوری"}],[{"text":"🌎 وضعیت سامانه"}]]}'''
            url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=⬇️ انتخاب کنید' + '&reply_markup=' + keyboard
            requests.get(url)

    def help_msg(self,chatid):
        find_session = db.session.find_one({'chatid':chatid})
        if find_session is None :
            help_text = '📙 راهنما' + '\n\n' + '🔹 برای ورود به سامانه و استفاده از خدمات ربات' + '\n' + 'باید اطلاعات ( نام کاربری و رمز عبور ) خود را با فرمت زیر ارسال کنید. 👇' + '\n\n' + \
            '<b>username:password</b>' + '\n\n' + '⬅️ نام کاربری = username' + '\n' + '⬅️ رمز عبور = password' + '\n\n' + 'برای مثال :' + '\n' + '<b>985193000:Abc123</b>' + \
            '\n\n' + 'بعد از وارد کردن اطلاعات میتوانید از خدمات ربات استفاده کنید.' + '\n\n'+ 'رمز عبور شما به هیچ وجه در سرور ذخیره نمیشود.' + '\n.'
        else:
            help_text = '📙 راهنما' + '\n\n' + '🗂 اطلاعات من' + '\n' + '🔸 در این بخش میتوانید اطلاعات خود را مشاهده کنید یا سشن خود را پاک کنید.' + '\n' + '❗️در صورت پاک شدن سشن باید مجددا وارد سامانه شوید.' + '\n\n' +\
            '📚 درس ها' + '\n' + '🔸 در این بخش میتوانید با انتخاب درس مورد نظر زمان برگزاری کلاس و آزمون های درس را مشاهده کنید.' + '\n\n' + \
            '🔔 یادآوری' + '\n' + '🔸 با فعال کردن این بخش ربات به صورت خودکار هر روز ساعت 6 صبح کلاس هایتان را به شما اطلاع رسانی میکند.' + '\n\n' + \
            '🗳 نظرات' + '\n' + '🔸در این بخش میتوانید ( انتقادات و پیشنهادات ) خود را به ادمین ارسال کنید.' + '\n\n' + '@RaziLMSbot\n.'

        url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text={help_text}&parse_mode=HTML'
        requests.get(url)



    def login_user(self,chatid,text):
        if db.session.find_one({'chatid':chatid}) is None:
            is_banned = db.banlist.find_one({'chatid':chatid})
            limit = db.user_limit(chatid)
            if limit is not True and is_banned is None:
                loading_text = 'در حال ورود به سامانه...'
                loading_url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + loading_text
                loading = requests.get(loading_url).json()['result']['message_id']
                try:
                    username = text.split(':')[0]
                    password = text.split(':')[1]
                    token = get_token(username, password)
                    delete_url = self.api_url + f'deleteMessage?chat_id={str(chatid)}&message_id={str(loading)}'
                    requests.get(delete_url)
                    if token is not False:
                        db.insert_session(chatid,token['.ASPXAUTH'],username,password)
                        login_detail = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + '✅ ورود به سامانه با موفقیت انجام شد.'
                        requests.get(login_detail)
                        self.start_msg(chatid)

                    else:
                        remaintries = db.userlimit.find_one({'chatid':chatid})['try']
                        remqintries = '\n\n' + 'تلاش های باقیمانده : ' + f'{str(remaintries)}/5\n.'
                        login_detail = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + '❗️اطلاعات اشتباه میباشد.' + remqintries
                        requests.get(login_detail)
                except Exception as e:
                    login_detail = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + '❗️‼️'
                    requests.get(login_detail)
            else:
                banned_user = self.api_url + f'sendMessage?chat_id={str(chatid)}&text='
                if is_banned is not None:
                    expiretime = datetime.datetime.utcnow() - is_banned['expire']
                    expireat = int((3600 - expiretime.total_seconds())/60)
                    banned_user += '❗️متاسفانه اکانت شما بن شده است.' + '\n' + '⏳ زمان باقیمانده : ' + str(expireat) + ' دقیقه' + '\n.'
                else:
                    banned_user += '❗️اکانت شما به مدت 1 ساعت بن شد.'
                    db.ban_list(chatid)
                requests.get(banned_user)
        else:
            login_detail = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + '❗️شما قبلا وارد شده اید.'
            requests.get(login_detail)

    def my_info(self,chatid):
        loading_text = 'در حال جستجو...'
        loading_url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + loading_text
        loading = requests.get(loading_url).json()['result']['message_id']
        token = db.session.find_one({'chatid':chatid})
        name = details({'.ASPXAUTH':token['session']})['name']
        self.delete_msg(chatid,loading)
        student_number = token['student_number']
        info_text = '🗂 اطلاعات من' + '\n' +  '🔰 شماره دانشجویی : '+ student_number + '\n' + '👤 نام : ' + name + '\n\n' + \
        '❗️توجه داشته باشید که با حذف سشن اکانت شما در بات حذف خواهد شد.' + '\n.'
        url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + info_text + '''&reply_markup={"inline_keyboard": [[{"text": "❌ حذف سشن","callback_data":"deletesession","selective":true}],[{"text": "📚 نمایش تمام کلاس‌ها","callback_data":"myallclass","selective":true}]]}'''
        requests.get(url)


    def my_lessons(self,chatid):
        loading_text = 'در حال جستجو...'
        loading_url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + loading_text
        loading = requests.get(loading_url).json()['result']['message_id']
        token = db.session.find_one({'chatid':chatid})
        courses = details({'.ASPXAUTH':token['session']})['lessons']
        delete_url = self.api_url + f'deleteMessage?chat_id={str(chatid)}&message_id={str(loading)}'
        requests.get(delete_url)
        keyboard = []
        if courses is not None:
            for course in courses:
                keyboard.append([{'text':'📖 ' + course['name'],'callback_data':'ls-' + course['link']}])
            inline_keyboard = json.dumps({'inline_keyboard':keyboard})
            course_text = '📚 لیست درس های اخذ شده.' + '\n\n' + 'برای مشاهده زمان و اطلاعات درس ها یکی از دکمه های زیر رانتخاب کنید. 👇'
            course_url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + course_text + '&reply_markup=' + str(inline_keyboard)


        else:
            course_text = '❗️متاسفانه درسی پیدا نشد.'
        requests.get(course_url)


    def myall_lessons(self,chatid):
        loading_text = 'در حال جستجو...'
        loading_url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + loading_text
        loading = requests.get(loading_url).json()['result']['message_id']
        token = db.session.find_one({'chatid':chatid})['session']
        textmsg = all_lessons(token,chatid)
        course_url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + textmsg
        requests.get(course_url)
        self.delete_msg(chatid,loading)

    def list_show(self,chatid,text):
        link = config.lms_url + 'Lesson' + text
        loading_text = 'در حال جستجو...'
        loading_url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + loading_text
        loading = requests.get(loading_url).json()['result']['message_id']
        token = db.session.find_one({'chatid':chatid})['session']
        simple = course_basic(token,link)
        self.delete_msg(chatid,loading)
        keyboard = []
        keyboard.append([{'text':'📘 کلاس','callback_data':'sl-' + text}])
        keyboard.append([{'text':'📕 آزمون','callback_data':'se-' + text}])
        keyboard.append([{'text':'📙 فایل های ضبط شده','callback_data':'re-' + text}])
        inline_keyboard = json.dumps({'inline_keyboard':keyboard})
        course_url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + simple + '&reply_markup=' + str(inline_keyboard)
        requests.get(course_url)

    def course_info(self,chatid,link,stat):
        loading_text = 'در حال جستجو...'
        loading_url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + loading_text
        loading = requests.get(loading_url).json()['result']['message_id']
        token = db.session.find_one({'chatid':chatid})
        if stat == 'coursedetail':
            link = config.lms_url + 'VirtualAdmin' + link
            info = course_detail({'.ASPXAUTH':token['session']},link)

        if stat == 'examdetail' :
            link = config.lms_url + 'Exam' + link
            info = my_exams({'.ASPXAUTH':token['session']},link)

        delete_url = self.api_url + f'deleteMessage?chat_id={str(chatid)}&message_id={str(loading)}'
        requests.get(delete_url)
        info_url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + info
        requests.get(info_url)



    def reminder(self,chatid,):
        stat = db.reminder_status.find_one({'chatid':chatid})
        if stat is not None:
            if stat['active'] :
                text = 'وضعیت : ✅ فعال' + '\n\n'
                keyboard = [[{"text": "🔕 غیرفعال","callback_data":"deactivereminder","selective":True}]]
            if stat['active'] is not True:
                text = 'وضعیت : ❗️ غیرفعال' + '\n\n'
                keyboard = [[{"text": "✅ فعالسازی","callback_data":"activereminder","selective":True}]]
        if stat is None:
            text = 'وضعیت : ❗️ غیرفعال' + '\n\n'
            keyboard = [[{"text": "✅ فعالسازی","callback_data":"activereminder","selective":True}]]
        text += 'با فعال کردن این بخش ربات کلاس هایتان را یادآوری میکند.' + '\n' + \
        'ساعت ارسال پیام در صورت داشتن کلاس 6 صبح میباشد.' + '\n\n' + '@RaziLMSbot' + '\n.'
        reminder_url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + urllib.parse.quote(text) + '&reply_markup=' + \
        urllib.parse.quote(str(json.dumps({'inline_keyboard':keyboard})))
        requests.get(reminder_url)

    def set_reminder(self,chatid,messageid,active):
        loading_text = 'لطفا صبر کنید...'
        loading_url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + loading_text
        loading = requests.get(loading_url).json()['result']['message_id']
        if active:
            db.set_reminder_status(chatid,True)
            token = db.session.find_one({'chatid':chatid})['session']
            #Thread(target = course_reminder,args = [token, chatid]).start()
            self.delete_msg(chatid,loading)
            text = '✅ یادآوری فعال شد.' + '\n\n'
        else:
            db.set_reminder_status(chatid,False)
            self.delete_msg(chatid,loading)
            text = '🔕 یادآوری غیرفعال شد.' + '\n\n'
        set_reminder_url = self.api_url + f'editMessageText?message_id={str(messageid)}&chat_id={str(chatid)}&text=' + urllib.parse.quote(text)
        requests.get(set_reminder_url)

    def delete_session(self,chatid,msgid):
        db.session.delete_one({'chatid':chatid})
        db.reminder_status.update_one({'chatid':chatid},{'$set':{'active':False}})
        delete_text = '✅ سشن با موفقیت حذف شد.'
        delete_url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + delete_text
        requests.get(delete_url)
        self.delete_msg(chatid,msgid)
        self.start_msg(chatid)

    def lms_statics(self,chatid):
        loading_text = 'لطفا صبر کنید...'
        loading_url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + loading_text
        loading = requests.get(loading_url).json()['result']['message_id']
        stats = RaziStatics.get_status_code()['stats']
        self.delete_msg(chatid,loading)
        text = str()
        for stat in stats:
            text += stat['link'] + ' ' + stat['text'] + '\n'
        url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + text
        requests.get(url)

    def recorded_list(self,chatid,link):
        link = config.lms_url + 'VirtualAdmin' + link
        link += 'iisijisjdas'
        removestr = '/' + link.split('/')[-1]
        link = link.replace(removestr,'')
        ulink = link.replace(config.lms_url + 'VirtualAdmin','')
        token = db.session.find_one({'chatid':chatid})
        LG = LinkGenerator(token['session'],link)
        allnames = LG.GetAllNames()
        if allnames is not None:
            keyboard = []
            for name in allnames:
                n = name['postback']
                n = n.replace('ctl00$mainContent','')
                keyboard.append([{'text':'📖 ' + name['date'],'callback_data': n + '|' + ulink}])

            inline_keyboard = json.dumps({'inline_keyboard':keyboard})
            course_text = 'انتخاب کنید 👇'
            course_url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + course_text + '&reply_markup=' + str(inline_keyboard)
            requests.get(course_url)
        else:
            text = '❗️نتیجه ای یافت نشد'
            course_url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + text
            requests.get(course_url)

    def get_recorded(self,chatid,text):
        loading_text = 'لطفا صبر کنید' + '\n' + 'در حال دریافت لینک...'
        loading_url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + loading_text
        loading = requests.get(loading_url).json()['result']['message_id']
        splited = text.split('|')
        postback = splited[0]
        link = config.lms_url + 'VirtualAdmin' + splited[1]
        token = db.session.find_one({'chatid':chatid})
        LG = LinkGenerator(token['session'],link)
        links = LG.GetPageVideo(postback)
        if len(links) != 0:
            text = '📖 نام درس : ' + links[0]['text'] + '\n\n' +\
             '❗️ برای مشاهده ویدیو روی لینک زیر کلیک کنید. 👇' + '\n\n'
            for link in links:
                url = link['link']
                name = link['name']
                text += f'<a href="{url}">👈 {name}</a>' + '\n\n'

            text += '@RaziLMSbot'
            url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + text + '&parse_mode=HTML'
        else:
            text = '❗️ نتیجه ای یافت نشد.'
            url = self.api_url + f'sendMessage?chat_id={str(chatid)}&text=' + text + '&parse_mode=HTML'

        self.delete_msg(chatid,loading)
        requests.get(url)




class channel(bot):

    def forward(self,chatid,messageid):
        if str(chatid) == config.channel_id:
            members = db.members.find()
            for member in members:
                to_id = member['chatid']
                forward_url = self.api_url + f'copyMessage?chat_id={to_id}&from_chat_id={str(chatid)}&message_id={messageid}'
                requests.get(forward_url)




bot = bot()
channel = channel()
