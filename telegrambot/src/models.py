from mongodata import mongo
import datetime
import time
from telegrambot.src.crypto import aescrypt
from dateutil.relativedelta import relativedelta


class db(mongo):
    def insert_session(self,chatid,session,stu_num,password):
        find = self.session.find_one({'chatid':chatid})
        exptime = datetime.datetime.now() + relativedelta(days=10)
        if find is None :
            hashedpw = aescrypt.encrypt(password)
            self.session.insert_one({'chatid':chatid,
            'session':session,'student_number':stu_num,'password':hashedpw,'expire_at':exptime})

    def insert_member(self,chatid):
        find = self.members.find_one({'chatid':chatid})
        if find is None:
            self.members.insert_one({'chatid':chatid,'timestamp':int(time.time())})

    def set_reminder_status(self,chatid,active):
        find = self.reminder_status.find_one({'chatid':chatid})
        if find is None:
            self.reminder_status.insert_one({'chatid':chatid,'active':active})
        else:
            self.reminder_status.update_one({'chatid':chatid},{'$set':{'active':active}})

    def user_limit(self,chatid):
        find = self.userlimit.find_one({'chatid':chatid})
        timestamp = datetime.datetime.utcnow()
        if find is not None:
            usertry = find['try'] + 1
            if usertry == 6 or usertry > 6 :
                return True
            else:
                self.userlimit.update_one({'chatid':chatid},{'$set':{'try': usertry}})
                return False

        else:
            self.userlimit.insert_one({'chatid':chatid,'try':1,'expire':timestamp})
            return False

    def ban_list(self,chatid):
        timestamp = datetime.datetime.utcnow()
        self.banlist.insert_one({'chatid':chatid,'expire':timestamp})

    def static_lists(self,stats):
        timestamp = datetime.datetime.utcnow()
        self.statics.insert_one({'stats':stats,'expire':timestamp})

db = db()
